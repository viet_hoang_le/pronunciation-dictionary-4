import {LOCALES, PRONUNCIATION} from './configs';

interface MyIPA {
  ipa?: string;
  word?: string;
  pron?: string;
}

export class Utils {

  constructor() {
  }

  static getInverseLocale = (locale: string): string =>
    locale == LOCALES.US.code ?
      LOCALES.GB.code : LOCALES.US.code;

  static getWordTypeByCode(code: string): string {
    let wordType = '';
    switch (parseInt(code)) {
      case 1:
        wordType = 'adv';
        break;
      case 2:
        wordType = 'pron';
        break;
      case 3:
        wordType = 'adj';
        break;
      case 4:
        wordType = 'verb';
        break;
      case 5:
        wordType = 'adj_adv';
        break;
      case 6:
        wordType = 'varnish';
        break;
      case 7:
        wordType = 'noun';
        break;
      case 10:
        wordType = 'unclassified';
        break;
      case 11:
        wordType = 'abbrv.';  // Abbreviation
        break;
      case 12:
        wordType = 'interj.'; // Interjection
        break;
      case 13:
        wordType = 'conj.';   // Conjunction
        break;
      case 14:
        wordType = 'pref.';   // Prefix
        break;
      case 15:
        wordType = 'prep.';        // Preposition
        break;
      case 16:
        wordType = 'indefinite article';        // Indefinite article
        break;
      default:
        break;
    }
    return wordType;
  }

  static getPronunciation(code: string): MyIPA {

    const ipa: MyIPA = {};

    if (PRONUNCIATION.consonants[code]
      || PRONUNCIATION.vowels.US[code]
      || PRONUNCIATION.vowels.GB[code]) {

      ipa['ipa'] = code;
      if (PRONUNCIATION.consonants[code]) {
        ipa['word'] = PRONUNCIATION.consonants[code].word;
        ipa['pron'] = PRONUNCIATION.consonants[code].pron;
      }
      else if (PRONUNCIATION.vowels.US[code]) {
        ipa['word'] = PRONUNCIATION.vowels.US[code].word;
      }
      else if (PRONUNCIATION.vowels.GB[code]) {
        ipa['word'] = PRONUNCIATION.vowels.GB[code].word;
      }
    }
    return ipa;
  }

  static htmlDecode = (input: string): string =>
    input ? String(input).replace(/<[^>]+>/gm, '') : '';

}
