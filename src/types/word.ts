import {Utils} from '../utils';

interface WordInterface {
  wordId: number;
  word: string;
  ipa: {
    US?: string,
    GB?: string,
  };
  type?: string;
  typeLabel?: string;
  timestamp?: number;
  isBookmarked?: boolean;

  setIPA(locale: string, ipa: string): void;
}

class Word implements WordInterface {

  private _wordId: number;
  private _word: string;
  private _ipa: { US?: string; GB?: string; };
  private _type?: string;
  private _typeLabel?: string;
  private _timestamp?: number;
  private _isBookmarked: boolean;

  constructor(wordId: number, word: string) {
    this._wordId = wordId;
    this._word = word;
    this._ipa = {};
  }

  get wordId(): number {
    return this._wordId;
  }

  set wordId(value: number) {
    this._wordId = value;
  }

  get word(): string {
    return this._word;
  }

  set word(value: string) {
    this._word = value;
  }

  get ipa(): { US?: string; GB?: string } {
    return this._ipa;
  }

  set ipa(value: { US?: string; GB?: string }) {
    this._ipa = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
    this._typeLabel = value ? Utils.getWordTypeByCode(value) : '';
  }

  get timestamp(): number {
    return this._timestamp;
  }

  set timestamp(value: number) {
    this._timestamp = value;
  }

  get isBookmarked(): boolean {
    return this._isBookmarked;
  }

  set isBookmarked(value: boolean) {
    this._isBookmarked = value;
  }

  get typeLabel(): string {
    return this._typeLabel;
  }

  setIPA(locale: string, ipa: string) {
    this._ipa[locale] = ipa;
  }
}

interface WordList {
  [wordId: number] : Word;
}

export {Word, WordList}
