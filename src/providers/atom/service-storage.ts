import {Injectable} from '@angular/core';
import {Platform, Events, AlertController} from 'ionic-angular';
import {Transfer, TransferObject} from '@ionic-native/transfer';
import {File, RemoveResult} from '@ionic-native/file';
import {NativeStorage} from '@ionic-native/native-storage';
import {SQLite, SQLiteDatabaseConfig, SQLiteObject} from '@ionic-native/sqlite';
import {Zip} from '@ionic-native/zip';
import {Word} from '../../types/word';
import {PLATFORM, TABLE, STORE, DEFAULT, CONFIGS, DICTS} from '../../configs';

const DB_DATA: SQLiteDatabaseConfig = {
  name: 'data.db',
  location: 'default'
};

export const EVENT_DATABASE_READY = 'database:ready';
export const EVENT_STORAGE_READY = 'storage_configs:ready';
export const EVENT_STORAGE_UPDATED = 'storage_configs:changed';

export interface ISetting {
  locale: string;
  locales: string[];
}

@Injectable()
export class ServiceStorage {

  private localDB: SQLiteObject;

  setting: ISetting = {
    locale: DEFAULT.locale,
    locales: DEFAULT.locales
  };

  public isSettingReady = false;
  public isDictReady = false;
  public version: string;
  public isWordRandom = true;
  public isPopoverShow: boolean;
  public isIPA_display: boolean;
  public speed: number;
  public fontsize: number;
  public color: string;
  public isDualTranscript: boolean;

  constructor(private platform: Platform,
              private events: Events,
              private alertCtrl: AlertController,
              private zip: Zip,
              private nativeStorage: NativeStorage,
              private sqlite: SQLite,
              private transfer: Transfer,
              private file: File) {
    this.version = '';
    this.isWordRandom = DEFAULT.isRandom;
    this.isPopoverShow = DEFAULT.isPopover;
    this.isIPA_display = DEFAULT.isIPA;
    this.speed = DEFAULT.speed;
    this.fontsize = DEFAULT.fontsize;
    this.isDualTranscript = DEFAULT.isDual;
    this.color = DEFAULT.color;
  }

  loadingConfiguration() {
    this.sqlite.create(DB_DATA).then((db: SQLiteObject) => {
      this.localDB = db;
      const query1 = 'CREATE TABLE IF NOT EXISTS history (wordId INTEGER PRIMARY KEY, word TEXT, type TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP);';
      const query2 = 'CREATE TABLE IF NOT EXISTS bookmark (wordId INTEGER PRIMARY KEY, word TEXT, type TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP);';
      this.localDB.sqlBatch([query1, query2,])
        .catch(e => console.error('initDB FAILED ! SQL batch ERROR: ', e));
    }).catch(e => console.error('ServiceStorage openDatabase ERROR = ', e));

    this.loadStoredConfigs();
  }

  /**
   * download the database zip file from remote server
   * return the location of downloaded zip file
   * @returns {Promise<string>}
   */
  downloadDb(): Promise<string> {
    const fileTransfer: TransferObject = this.transfer.create();
    const url_primary = DICTS.remote_deploy.url_primary;
    const fileName = DICTS.remote_deploy.file_name;
    return new Promise<string>(resolve => {
      fileTransfer.download(url_primary, this.file.dataDirectory + fileName).then((entry) => {
        resolve(entry.toURL());
      }, (error) => {
        // attempts to download again with the alternative url
        const url_minor = DICTS.remote_deploy.url_minor;
        fileTransfer.download(url_minor, this.file.dataDirectory + fileName).then((entry) => {
          resolve(entry.toURL());
        }, (error) => {
          // handle the error
          console.error('ERROR downloadDb = ' + JSON.stringify(error));
          resolve(null);
        });
      });
    });
  }

  private deleteZip() {
    this.file.removeFile(this.file.dataDirectory, DICTS.remote_deploy.file_name).then((result: RemoveResult) => {
      if (!result.success)
        console.error('ERROR deleteZip = ' + result.fileRemoved.name)
    });
  }

  /**
   * Unzip the db.zip file into the app databases
   * @param zipFilePath
   * @param dbFilename
   * @returns {Promise<boolean>}
   */
  unzipDB(zipFilePath: string): Promise<boolean> {
    const fs = this.file.applicationStorageDirectory;
    const db_location_ios = fs + 'Library/LocalDatabase';
    const db_location_android = fs + 'databases';
    const destPath = (this.platform.is(PLATFORM.ios) ? db_location_ios : db_location_android) + '/';

    return new Promise<boolean>(resolve => {
      this.zip.unzip(zipFilePath, destPath, (progress) => console.log('Unzipping = ' + Math.round((progress.loaded / progress.total) * 100) + '%'))
        .then((result) => {
          if (result === 0) {
            this.deleteZip();
            resolve(true);
          }
          if (result === -1) {
            console.error('FAILED = ' + zipFilePath);
            this.deleteZip();
            resolve(false);
          }
        });
    });
  }

  isDbDeployed(): Promise<boolean> {
    const fs = this.file.applicationStorageDirectory;
    const db_location_ios = fs + 'Library/LocalDatabase';
    const db_location_android = fs + 'databases';
    const destPath = (this.platform.is(PLATFORM.ios) ? db_location_ios : db_location_android) + '/';
    const checkFiles = [
      this.file.checkFile(destPath, DICTS.CMU_07b.dbFileName),
      this.file.checkFile(destPath, DICTS.CUV2.dbFileName)
    ];
    return new Promise(resolve => {
      Promise.all(checkFiles).then(() => {
        resolve(true);
      }, errs => {
        console.error('checkFile error = ' + JSON.stringify(errs));
        resolve(false);
      });
    });
  }

  private publishReady(goal: number, index: number) {
    // console.log(index + ' ==> ' + goal);
    if (index === goal) {
      this.isSettingReady = true;
      this.events.publish(EVENT_STORAGE_READY);
      this.updateFontsize();
    }
  }

  loadStoredConfigs() {
    const goal = 0;
    let index = 0;
    this.nativeStorage.getItem(STORE.version).then(
      data => {
        this.version = data;
        this.publishReady(goal, index++);
      },
      error => {
        this.publishReady(goal, index++);
      });
    this.nativeStorage.getItem(STORE.isRandom).then(
      data => {
        this.isWordRandom = JSON.parse(data);
        this.publishReady(goal, index++);
      },
      error => {
        this.isWordRandom = DEFAULT.isRandom;
        this.publishReady(goal, index++);
      });
    this.nativeStorage.getItem(STORE.locale).then(
      data => {
        this.setting.locale = data;
        this.publishReady(goal, index++);
      },
      error => {
        this.setting.locale = DEFAULT.locale;
        this.publishReady(goal, index++);
      });
    this.nativeStorage.getItem(STORE.locales).then(
      data => {
        this.setting.locales = data.split(',');
        this.publishReady(goal, index++);
      },
      error => {
        // do nothing here
        this.setting.locales = DEFAULT.locales;
        this.publishReady(goal, index++);
      });
    this.nativeStorage.getItem(STORE.isPopover).then(
      data => {
        this.isPopoverShow = JSON.parse(data);
        this.publishReady(goal, index++);
      },
      error => {
        this.isPopoverShow = DEFAULT.isPopover;
        this.publishReady(goal, index++);
      });
    this.nativeStorage.getItem(STORE.isIPA).then(
      data => {
        this.isIPA_display = JSON.parse(data);
        this.publishReady(goal, index++);
      },
      error => {
        this.isIPA_display = DEFAULT.isIPA;
        this.publishReady(goal, index++);
      });
    this.nativeStorage.getItem(STORE.speed).then(
      data => {
        this.speed = JSON.parse(data);
        this.publishReady(goal, index++);
      },
      error => {
        this.speed = DEFAULT.speed;
        this.publishReady(goal, index++);
      });
    this.nativeStorage.getItem(STORE.fontsize).then(
      data => {
        this.fontsize = JSON.parse(data);
        this.publishReady(goal, index++);
      },
      error => {
        this.fontsize = DEFAULT.fontsize;
        this.publishReady(goal, index++);
      });
    this.nativeStorage.getItem(STORE.color).then(
      data => {
        this.color = data;
        this.publishReady(goal, index++);
      },
      error => {
        this.color = DEFAULT.color;
        this.publishReady(goal, index++);
      });
    this.nativeStorage.getItem(STORE.isDual).then(
      data => {
        this.isDualTranscript = JSON.parse(data);
        this.publishReady(goal, index++);
      },
      error => {
        this.isDualTranscript = DEFAULT.isDual;
        this.publishReady(goal, index++);
      });
  }

  setStorage(key: string, value: any) {
    this.nativeStorage.setItem(key, value);
    switch (key) {
      case STORE.version:
        this.version = value;
        break;
      case STORE.isPopover:
        this.isPopoverShow = value;
        break;
      case STORE.locale:
        this.setting.locale = value;
        break;
      case STORE.locales:
        this.setting.locales = value;
        break;
      case STORE.isRandom:
        this.isWordRandom = value;
        break;
      case STORE.isIPA:
        this.isIPA_display = value;
        break;
      case STORE.speed:
        this.speed = value;
        break;
      case STORE.fontsize:
        this.fontsize = value;
        this.updateFontsize();
        break;
      case STORE.color:
        this.color = value;
        break;
      case STORE.isDual:
        this.isDualTranscript = value;
        break;
      default:
        break;
    }
    this.events.publish(EVENT_STORAGE_UPDATED, key);
  }

  setDbReady() {
    this.isDictReady = true;
    this.events.publish(EVENT_DATABASE_READY);
  }

  deleteBookmarkHistory(tableName: string, wordId: number) {
    const query = 'DELETE FROM ' + tableName + ' WHERE wordId = ?';
    this.localDB.executeSql(query, [wordId]).then(() => {
    }, (error) => {
      console.error('deleteBookmarkHistory from ' + tableName + ' error = ' + JSON.stringify(error));
    });
  }

  deleteAllBookmarkHistory(tableName: string) {
    const query = 'DELETE FROM ' + tableName + ';';
    this.localDB.executeSql(query, []).then(() => {
    }, (error) => {
      console.error('deleteAllBookmarkHistory from ' + tableName + ' error = ' + JSON.stringify(error));
    });
  }

  deleteLimitHistory(limit: number) {
    const querySelect = 'SELECT wordId FROM history ORDER BY timestamp DESC LIMIT ?,10;';
    this.localDB.executeSql(querySelect, [limit - 1]).then(result => {
      const params = [];
      for (let i = 0; i < result.rows.length; i++) {
        params.push(result.rows.item(i).wordId);
      }
      if (params.length > 0) {
        const queryDelete = 'DELETE FROM history WHERE wordId IN (' + params + ');';
        this.localDB.executeSql(queryDelete, []).then(result => {
        }, (error) => {
          console.error('delete history error = ' + JSON.stringify(error));
        });
      }
    }, (error) => {
      console.error('delete history error = ' + JSON.stringify(error));
    });
  }

  setBookmarkHistory(table: string, wordItem: Word, limit = CONFIGS.history_limit) {
    if (table == TABLE.HISTORY && limit)
      this.deleteLimitHistory(limit);

    const query = 'INSERT OR IGNORE INTO ' + table + ' (wordId, word, type) VALUES(?,?,?) ; ';
    this.localDB.executeSql(query, [wordItem.wordId, wordItem.word, wordItem.type]).then(value => {
      // console.log('setBookmarkHistory = ' + JSON.stringify(value));
    }, (error) => {
      console.error('setBookmarkHistory error = ' + JSON.stringify(error));
    });
  }

  loadBookmarkHistory(table: string, limit = 0): Promise<Word[]> {
    const bookmarkHistoryArr: Word[] = [];
    let query = 'SELECT wordId, word, type, timestamp FROM ' + table + ' ORDER BY timestamp DESC';
    if (limit > 0) query = query + ' LIMIT 0, ' + limit;
    return new Promise<any[]>(resolve => {
      this.localDB.executeSql(query, []).then(result => {
        // console.log('loadBookmarkHistory result = ' + JSON.stringify(result));
        for (let i = 0; i < result.rows.length; i++) {
          const aWord = new Word(result.rows.item(i).wordId, result.rows.item(i).word);
          aWord.type = result.rows.item(i).type;
          aWord.timestamp = result.rows.item(i).timestamp;
          bookmarkHistoryArr.push(aWord);
        }
        resolve(bookmarkHistoryArr);
      }, (error) => {
        console.error('loadBookmarkHistory error = ' + JSON.stringify(error));
        resolve(bookmarkHistoryArr);
      });
    });
  }

  checkingBookmarks(wordIds: number[] | string[]): Promise<any[]> {
    const bookmarkedWordIds: number[] = [];
    const query = 'SELECT wordId FROM bookmark WHERE wordId IN (' + wordIds + ');';
    return new Promise<any[]>(resolve => {
      this.localDB.executeSql(query, []).then(result => {
        for (let i = 0; i < result.rows.length; i++)
          bookmarkedWordIds.push(result.rows.item(i).wordId);
        resolve(bookmarkedWordIds);
      }, (error) => {
        console.error('checkingBookmarks error = ' + JSON.stringify(error));
        resolve(bookmarkedWordIds);
      });
    });
  }

  updateFontsize() {
    const html = document.getElementsByTagName('html');
    if (html.length > 0)
      html.item(0).style.fontSize = this.fontsize + '%';
  }

  presentConfirm(title: string, message: string, handleConfirmJob: any): void {
    const alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Continue...',
          handler: () => handleConfirmJob()
        }
      ]
    });
    alert.present();
  }

  isLatestVersion = (): boolean => this.version.indexOf(CONFIGS.app_version) > -1;

}

