import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {SQLite, SQLiteDatabaseConfig, SQLiteObject} from '@ionic-native/sqlite';
import {ServiceStorage} from './atom/service-storage';
import {CONFIGS, DICTS, LOCALES} from '../configs';
import {Word, WordList} from '../types/word';

const DB_US: SQLiteDatabaseConfig = {
  name: DICTS.CMU_07b.dbFileName,
  location: CONFIGS.db_location_default
};
const DB_GB: SQLiteDatabaseConfig = {
  name: DICTS.CUV2.dbFileName,
  location: CONFIGS.db_location_default
};

@Injectable()
export class ServiceDictionary {
  dictUS: SQLiteObject;
  dictGB: SQLiteObject;

  constructor(private sqlite: SQLite,
              public serviceStorage: ServiceStorage) {
  }

  openDatabase(): Promise<boolean> {
    return new Promise(resolve => {
      this.serviceStorage.isDbDeployed().then(isDeployed => {
        if (isDeployed) {
          Promise.all([
            this.sqlite.create(DB_US),
            this.sqlite.create(DB_GB)
          ]).then((values) => {
            this.dictUS = values[0];
            this.dictGB = values[1];
            resolve(true);
          }, (errs) => {
            console.error('ServiceDictionary openDatabase ERROR = ', errs);
            resolve(false);
          });
        }
        else
          resolve(false);
      });
    });
  }

  searchWords(searchText, index, limit, isRandom, locale): Promise<WordList> {
    const wordListObj: WordList = {};
    let currentDict = this.dictGB;
    let max = 1,
      min = 1;
    let query = '';
    let parameters = [index, limit];

    return new Promise(resolve => {
      if (this.dictUS
        && locale == LOCALES.US.code) {
        query = 'SELECT wordId, word, ipa FROM word';
        currentDict = this.dictUS;
        max = DICTS.CMU_07b.count;
      }
      else if (this.dictGB
        && (locale == LOCALES.GB.code
        || locale == LOCALES.AU.code)) {
        query = 'SELECT wordId, word, ipa, type FROM word';
        max = DICTS.CUV2.count;
      }
      else {
        resolve(wordListObj);
      }
      if (currentDict) {
        if (searchText && searchText.length > 0) {
          let queryText = searchText.replace(/\s/ig, '%') + '%';
          query += ' WHERE word LIKE ? LIMIT ?,?';
          parameters = [queryText, index, limit];
        }
        else if (isRandom == true) {
          let randomIds: number[] = [],
            randomId: number;
          while (randomIds.length < limit) {
            randomId = Math.floor(Math.random() * (max - min)) + min;
            if (randomIds.indexOf(randomId) < 0)
              randomIds.push(randomId);
          }
          query += ' WHERE wordId IN (' + randomIds + ') LIMIT ?,?';
        } else {
          query += ' LIMIT ?,?';
        }
        currentDict.executeSql(query, parameters).then(result => {
          for (let i = 0; i < result.rows.length; i++) {
            const aWord = new Word(result.rows.item(i).wordId, result.rows.item(i).word);
            aWord.type = result.rows.item(i).type;
            aWord.setIPA(locale, result.rows.item(i).ipa);
            wordListObj[aWord.wordId] = aWord;
          }
          resolve(wordListObj);
        }, (err) => {
          console.error('Unable to execute sql searchWords = ' + JSON.stringify(err));
          resolve(wordListObj);
        });
      }
      else {
        resolve(wordListObj);
      }
    });
  }

  loadPronunciations(wordList: WordList, locale: string, isItemsAdd = false): Promise<boolean> {

    const currentDict = locale == LOCALES.US.code ? this.dictUS : this.dictGB;

    let parameters = [];
    for (const key in wordList) {
      const aWord: Word = wordList[key];
      if (!aWord.ipa)
        aWord.ipa = {};
      parameters.push(aWord.word);
    }
    return new Promise(resolve => {
      if (parameters.length > 0) {
        let queryInlineParams = '';
        const queryInlineArr = [];
        for (const param of parameters) {
          queryInlineParams += ',?';
          queryInlineArr.push(param.toLowerCase());
          if (param.includes(' ')) {
            queryInlineParams += ',?';
            queryInlineArr.push(param.toLowerCase().replace(/\s/g, '-'));
          }
        }
        queryInlineParams = queryInlineParams.substr(1);
        const query = 'SELECT wordId, word, ipa FROM word WHERE LOWER(word) IN (' + queryInlineParams + ');';
        currentDict.executeSql(query, queryInlineArr).then(result => {
          for (let i = 0; i < result.rows.length; i++) {
            const wordId = result.rows.item(i).wordId;
            const word = result.rows.item(i).word.toLowerCase();
            const ipa = result.rows.item(i).ipa;
            for (const key of Object.keys(wordList)) {
              const aWord: Word = wordList[key];
              const lWord = aWord.word.toLowerCase();
              if (lWord == word || lWord.replace(/\s/g, '-') == word) {
                if (!aWord.ipa[locale] || aWord.ipa[locale].length == 0)
                  aWord.ipa[locale] = ipa;
              }
            }
            if (isItemsAdd && !wordList[wordId]) {
              const aWord = new Word(wordId, result.rows.item(i).word);
              aWord.setIPA(locale, ipa);
              wordList[wordId] = aWord;
            }
          }
          resolve(true);
        }, (error) => {
          console.error('usPronunciationDb - loadPronunciations query error = ' + JSON.stringify(error));
          resolve(false);
        });
      }
      else {
        resolve(true);
      }
    });
  }

}

