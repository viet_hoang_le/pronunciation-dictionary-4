import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {App, NavController, PopoverController, Searchbar} from 'ionic-angular';
import {ServiceStorage} from '../../providers/atom/service-storage';
import {ServiceHelper} from '../../providers/service-helper';

@Component({
  selector: 'navBar',
  templateUrl: 'navbar.component.html'
})
export class Navbar {

  @Input() title = 'Bibooki';
  @Input() hasSearch = false;
  @Input() hasSearchTitle = false;  // apply for chapter title & search in chapter
  @Input() hasPopLocale = false;
  @Input() hasPopControl = false;
  @Input() hasPopHistory = false;
  @Input() hasReturn = true;
  @Input() searchText = '';

  @Output() onSearch = new EventEmitter<string>();

  @ViewChild(Searchbar) searchbar: Searchbar;

  isSearchActive = false;
  setting = this.serviceStorage.setting;

  constructor(
    private app: App,
    private navCtrl: NavController,
    private popoverCtrl: PopoverController,
    private serviceStorage: ServiceStorage,
    private serviceHelper: ServiceHelper) {

  }

  onclick_popoverLocale(ev) {
    this.serviceHelper.onclickPopoverLocale(ev);
  }

  onclick_popoverControl(myEvent) {
    const popover = this.popoverCtrl.create('popover-control');
    popover.present({ ev: myEvent });
  }

  onclick_popoverHistory(myEvent) {
    const popover = this.popoverCtrl.create('popover-history');
    popover.present({ ev: myEvent });
  }

  onclick_searchInTitle() {
    if (this.hasSearchTitle) {
      this.hasSearch = true;
      this.isSearchActive = true;
      setTimeout(() => this.searchbar.setFocus());
    }
  }
  onSearchInput(ev) {
    const val = ev.target.value;
    this.searchText = val && val.trim() != '' ? val : '';
    this.onSearch.emit(this.searchText);
  }
  onSearchClear() {
    this.searchText = '';
    this.onSearch.emit(this.searchText);
  }
  onSearchFocus() {
    this.isSearchActive = true;
  }
  onSearchBlur() {
    if (this.hasSearchTitle) {
      this.hasSearch = false;
    }
    this.isSearchActive = false;
  }

  returnHome() {
    if (this.navCtrl.length() > 1)
      this.navCtrl.popToRoot();
    else
      this.app.getRootNav().setRoot('home');
  }
}
