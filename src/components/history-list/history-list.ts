import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {
  EVENT_STORAGE_UPDATED,
  ServiceStorage
} from '../../providers/atom/service-storage';
import {Word, WordList} from '../../types/word';
import {STORE, TABLE} from '../../configs';
import {ServiceHelper} from '../../providers/service-helper';
import {Events} from 'ionic-angular';
import {ServiceDictionary} from '../../providers/service-dictionary';

@Component({
  selector: 'history-list',
  templateUrl: 'history-list.html',
})
export class HistoryListComponent implements OnInit, OnDestroy {

  @Input() limit?: number;

  items: Word[];
  itemList: WordList = {};
  isIPA_display = true;
  setting = this.serviceStorage.setting;

  constructor(
    private events: Events,
    private serviceHelper: ServiceHelper,
    private serviceDictionary: ServiceDictionary,
    private serviceStorage: ServiceStorage,
  ) {

    this.isIPA_display = this.serviceStorage.isIPA_display;

    this.setupHistoryList();
  }

  ngOnInit(): void {
    this.events.subscribe(EVENT_STORAGE_UPDATED, args => {
      if (args === STORE.locale) {
        this.serviceDictionary.loadPronunciations(this.itemList, this.serviceStorage.setting.locale);
      }
    });
  }

  ngOnDestroy(): void {
    this.events.unsubscribe(EVENT_STORAGE_UPDATED);
  }

  private setupHistoryList() {
    this.serviceStorage.loadBookmarkHistory(TABLE.HISTORY).then(result => {
      this.items = result;
      for (const item of this.items) {
        this.itemList[item.wordId] = item;
      }
      this.serviceDictionary.loadPronunciations(this.itemList, this.setting.locale);

      this.serviceStorage.checkingBookmarks(Object.keys(this.itemList)).then(bookmarkedIds => {
        for (let id of bookmarkedIds)
          this.itemList[id].isBookmarked = true;
      });
    });
  }

  onclick_toggleBookmark(item: Word) {
    item.isBookmarked = !item.isBookmarked;
    this.serviceStorage.setBookmarkHistory(TABLE.BOOKMARK, item);
  }

  onclick_word(item: Word) {
    this.serviceHelper.onclickWord(item);
  }
}
