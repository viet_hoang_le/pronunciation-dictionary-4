import {Component, ElementRef, ViewChild, OnInit, OnDestroy} from '@angular/core';
import {NavController, Events, IonicPage} from 'ionic-angular';
import {ServiceHelper} from '../../../providers/service-helper';
import {ServiceDictionary} from '../../../providers/service-dictionary';
import {EVENT_STORAGE_UPDATED, ServiceStorage} from '../../../providers/atom/service-storage';
import {COLORS, STORE, TABLE} from '../../../configs';
import {Word, WordList} from '../../../types/word';

@IonicPage({
  name: 'tab-bookmark'
})
@Component({
  templateUrl: 'tab-bookmark.html',
})
export class TabBookmarkPage implements OnInit, OnDestroy {

  setting = this.serviceStorage.setting;
  public items: Word[] = [];
  public itemList: WordList = {};
  public isIPA_display: boolean = true;
  public isHideDeleteBtn: boolean = true;

  @ViewChild('skinContent', {read: ElementRef}) skinContent: ElementRef;

  constructor(private nav: NavController,
              private events: Events,
              private serviceHelper: ServiceHelper,
              private serviceDictionary: ServiceDictionary,
              private serviceStorage: ServiceStorage) {

  }

  ngOnInit(): void {
    this.serviceHelper.trackPage('Bookmark_Tab_page');

    this.isIPA_display = this.serviceStorage.isIPA_display;

    this.serviceStorage.loadBookmarkHistory(TABLE.BOOKMARK, 0).then(result => {
      this.items = result;
      for (let word of this.items) {
        this.itemList[word.wordId] = word;
      }
      this.serviceDictionary.loadPronunciations(this.itemList, this.setting.locale);
    });

    setTimeout(() => {
      let color = this.serviceStorage.color;
      this.skinContent.nativeElement.style.backgroundColor = COLORS[color].bg;
      this.skinContent.nativeElement.style.color = COLORS[color].color;
    }, 500);

    this.events.subscribe(EVENT_STORAGE_UPDATED, args => {
      if (args === STORE.locale)
        this.serviceDictionary.loadPronunciations(this.itemList, this.setting.locale);
    });
  }
  ngOnDestroy(): void {
    this.events.unsubscribe(EVENT_STORAGE_UPDATED);
  }

  onclick_word = (wordItem: Word) => this.serviceHelper.onclickWord(wordItem);

  onclick_popoverLocale = (ev) => this.serviceHelper.onclickPopoverLocale(ev);

  onclick_clearAll(ev) {

    const title = 'Delete all Bookmarks ?';
    const message = 'Once deleted, all your bookmarks will be gone.';
    this.serviceStorage.presentConfirm(title, message, () => {
      this.serviceStorage.deleteAllBookmarkHistory(TABLE.BOOKMARK);
      this.items = [];
      this.itemList = {};
    });
  }

  onclick_delete(wordItem) {

    delete this.itemList[wordItem.wordId];
    this.items = [];
    for (let key of Object.keys(this.itemList)) {
      this.items.push(this.itemList[key]);
    }
    this.serviceStorage.deleteBookmarkHistory(TABLE.BOOKMARK, wordItem.wordId);

  }

  onclick_toggleDelete(ev) {

    this.isHideDeleteBtn = !this.isHideDeleteBtn;

  }

}
