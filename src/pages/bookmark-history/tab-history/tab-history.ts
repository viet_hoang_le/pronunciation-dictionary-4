import {Component, ElementRef, ViewChild, OnInit} from '@angular/core';
import {App, IonicPage} from 'ionic-angular';
import {ServiceHelper} from '../../../providers/service-helper';
import {ServiceStorage} from '../../../providers/atom/service-storage';
import {COLORS, TABLE} from '../../../configs';

@IonicPage({
  name: 'tab-history'
})
@Component({
  templateUrl: 'tab-history.html',
})
export class TabHistoryPage implements OnInit {

  setting = this.serviceStorage.setting;

  @ViewChild('skinContent', {read: ElementRef}) skinContent: ElementRef;

  constructor(
    private appCtrl: App,
    private serviceHelper: ServiceHelper,
    private serviceStorage: ServiceStorage
  ) {
  }

  ngOnInit(): void {
    this.serviceHelper.trackPage('History_Tab_page');

    setTimeout(() => {
      let color = this.serviceStorage.color;
      this.skinContent.nativeElement.style.backgroundColor = COLORS[color].bg;
      this.skinContent.nativeElement.style.color = COLORS[color].color;
    }, 500);
  }

  ngOnDestroy(): void {
  }

  onclick_popoverLocale(ev) {
    this.serviceHelper.onclickPopoverLocale(ev);
  }

  onclick_clearAll(ev) {
    const title = 'Delete all Histories ?';
    const message = 'Once deleted, all your histories will be gone.';
    this.serviceStorage.presentConfirm(title, message, () => {
      this.serviceStorage.deleteAllBookmarkHistory(TABLE.HISTORY);
      this.appCtrl.getRootNav().setRoot('home');
    });
  }
}
