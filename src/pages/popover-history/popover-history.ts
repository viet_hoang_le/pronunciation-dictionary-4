import {Component, OnInit} from '@angular/core';
import {App, IonicPage, ViewController} from 'ionic-angular';
import {ServiceHelper} from '../../providers/service-helper';

@IonicPage({
  name: 'popover-history'
})
@Component({
  templateUrl: './popover-history.html',
})
export class PopoverHistoryPage implements OnInit {

  constructor(
    private viewCtrl: ViewController,
    private appCtrl: App,
    private serviceHelper: ServiceHelper,
  ) {
  }

  ngOnInit(): void {
    this.serviceHelper.trackPage('History_popover');
  }

  onclick_history() {
    this.viewCtrl.dismiss();
    this.appCtrl.getRootNav().setRoot('bookmark-history');
  }

}
