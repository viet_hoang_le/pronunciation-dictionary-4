import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {WordDetailPage} from './word-detail';
import {NavbarModule} from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    WordDetailPage,
  ],
  imports: [
    NavbarModule,
    IonicPageModule.forChild(WordDetailPage),
  ],
  entryComponents: [
    WordDetailPage
  ]
})
export class WordDetailModule {}
