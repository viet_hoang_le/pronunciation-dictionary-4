import {Component, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import {NavParams, Events, IonicPage} from 'ionic-angular';
import {ServiceDictionary} from '../../providers/service-dictionary';
import {ServiceHelper} from '../../providers/service-helper';
import {ServiceStorage} from '../../providers/atom/service-storage';
import {COLORS, LOCALES, PRONUNCIATION, TABLE} from '../../configs';
import {Utils} from '../../utils';
import {WordList} from '../../types/word';

@IonicPage({
  name: 'word-detail'
})
@Component({
  templateUrl: 'word-detail.html',
})
export class WordDetailPage implements AfterViewInit {

  word;
  ipaList = {
    US: [],
    GB: [],
    AU: [],
  };
  localeArr = ['US', 'GB', 'AU'];
  toggle = {
    US: true,
    GB: true,
    AU: true
  };

  @ViewChild('skinContent', {read: ElementRef}) skinContent: ElementRef;

  constructor(private events: Events,
              private params: NavParams,
              private serviceStorage: ServiceStorage,
              private serviceHelper: ServiceHelper,
              private serviceDictionary: ServiceDictionary) {

    this.word = this.params.get('item');

    const wordList: WordList = {};
    wordList[this.word.wordId] = this.word;

    this.serviceDictionary.loadPronunciations(wordList, LOCALES.US.code).then(() => {
      this.update_ipa_list(LOCALES.US.code);
    });

    this.serviceDictionary.loadPronunciations(wordList, LOCALES.GB.code).then(() => {
      this.update_ipa_list(LOCALES.GB.code);
    });

  }

  ngAfterViewInit(): void {
    this.serviceHelper.trackPage('Word_Detail_page');

    setTimeout(() => {
      let color = this.serviceStorage.color;
      this.skinContent.nativeElement.style.backgroundColor = COLORS[color].bg;
      this.skinContent.nativeElement.style.color = COLORS[color].color;
    }, 500);
  }

  update_ipa_list(locale: string) {
    let ipa_char = '';
    let longest_char_number = PRONUNCIATION.longest_char_number;

    let word_ipa = this.word.ipa[locale];
    if (word_ipa) {
      word_ipa = word_ipa.replace(/[',]+/g, '');
      for (let i = 0; i < word_ipa.length; i++) {
        for (let k = longest_char_number; k > 0; k--) {
          if (i + k <= word_ipa.length) {
            ipa_char = word_ipa.substr(i, k);
            const ipa = Utils.getPronunciation(ipa_char);
            if (ipa.word) {
              this.ipaList[locale].push(ipa);
              i += k;
              i--;  // because that we already increased i to new position, so we don't need to increase by loop 'for'.
              break;
            }
          }
        }
      }
    }
  }

  onclick_word(word, locale) {
    this.serviceHelper.tts_text(word, locale);
  }

  onclick_word_natural(word, locale) {
    this.serviceHelper.naturalTTS(word, locale);
  }

  onclick_toggleBookmark() {

    this.word.isBookmarked = !this.word.isBookmarked;

    if (this.word.isBookmarked) {
      this.serviceStorage.setBookmarkHistory(TABLE.BOOKMARK, this.word);
    } else {
      this.serviceStorage.deleteBookmarkHistory(TABLE.BOOKMARK, this.word.wordId);
    }

  }

}
