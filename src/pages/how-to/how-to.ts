import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';

@IonicPage({
  name: 'howto'
})
@Component({
  selector: 'page-how-to',
  templateUrl: 'how-to.html',
})
export class HowTo {

  device_type: string;
  tips: string;
  link: string;

  constructor(private navCtrl: NavController,
              private plt: Platform,
              private navParams: NavParams) {

    this.tips = 'If you cannot hear the correct pronunciations of US - GB - AU. ' +
      'Maybe your device have not installed the correct Text-To-Speech language packages. ' +
      'Please follow the instructions in below link to install the needed packages ' +
      'and enjoy the full feature of our Pronunciation Dictionary';

    if (this.plt.is('ios')) {
      this.device_type = 'iPhone';
      this.link = 'https://support.apple.com/en-us/HT203077';
    }
    else {
      this.device_type = 'Android';
      this.link = 'http://www.androidauthority.com/google-text-to-speech-engine-659528/';
    }
  }

}
