import {Component, ViewChild, ElementRef, OnInit, AfterViewInit} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {ServiceStorage} from '../../providers/atom/service-storage';
import {Toast} from '@ionic-native/toast';
import {COLORS, STORE, DEFAULT, LOCALES} from '../../configs';
import {ServiceHelper} from '../../providers/service-helper';

@IonicPage({
  name: 'setting'
})
@Component({
  templateUrl: 'setting.html',
})
export class SettingPage implements OnInit, AfterViewInit {

  setting = this.serviceStorage.setting;
  
  isIPA_display = true;
  isPopoverShow = true;
  isWordRandom = true;
  locales_default: string[];
  jsonLocales;

  @ViewChild('skinContent', {read: ElementRef}) skinContent: ElementRef;

  constructor(private toast: Toast,
              private serviceStorage: ServiceStorage,
              private serviceHelper: ServiceHelper) {
    this.jsonLocales = LOCALES;
  }

  ngOnInit(): void {
    this.serviceHelper.trackPage('Setting_page');

    this.loadConfigs();

  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      let color = this.serviceStorage.color;
      this.skinContent.nativeElement.style.backgroundColor = COLORS[color].bg;
      this.skinContent.nativeElement.style.color = COLORS[color].color;
    }, 500);
  }

  public loadConfigs() {

    this.isIPA_display = this.serviceStorage.isIPA_display;
    this.locales_default = DEFAULT.locales_full;

    this.isPopoverShow = this.serviceStorage.isPopoverShow;
    this.isWordRandom = this.serviceStorage.isWordRandom;
  }

  toggleIPA_display = () => this.serviceStorage.setStorage(STORE.isIPA, this.isIPA_display);

  toggleHidePopover = () => this.serviceStorage.setStorage(STORE.isPopover, this.isPopoverShow);

  toggleWordRandom = () => this.serviceStorage.setStorage(STORE.isRandom, this.isWordRandom);

  toggleLocale(code) {
    let locales: string[] = this.setting.locales;
    if (locales.indexOf(code) > -1) {
      if (locales.length > 1) {
        locales.splice(locales.indexOf(code), 1);
      } else {
        this.toast.showShortBottom('You cannot deactivate all voices').subscribe();
        return;
      }
    } else {
      locales.push(code);
    }

    this.serviceStorage.setStorage(STORE.locales, locales);
  };
}
