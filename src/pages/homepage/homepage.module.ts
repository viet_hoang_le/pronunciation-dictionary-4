import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {HomePage} from './homepage';
import {NavbarModule} from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    NavbarModule,
    IonicPageModule.forChild(HomePage),
  ],
  entryComponents: [
    HomePage
  ]
})
export class HomePageModule {}
