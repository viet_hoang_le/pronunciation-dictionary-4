export const CONFIGS = {
  appName: 'Pronunciation Dictionary',
  description: 'Dependency Injection',
  app_version: '2017.06.06',
  rate_app_text: 'Leave us a comment to help others to know how this dictionary will help them.',
  copyright: 'Bibooki Mobile Studio @2017',
  homepage: 'http://bibooki.com',
  contact: 'mobile@bibooki.com',
  about_note: 'Your donation by <b>PAYPAL</b> to: <b>LHViet88 (@gmail.com)</b> will help us to maintain and to continue develop this application.<br>Thank you for your help.',
  feature_new: '<ul>' +
  '<li>Upgrade Natural & correct pronuciation (*internet required)</li>' +
  '<li>US & UK transcripts display at the same time</li>' +
  '<li>Highlighting current word</li>' +
  '<li>Improved performance of the app</li>' +
  '<li>Redesign to increase the display space</li>' +
  '</ul>',
  feature_basic: '<ul>' +
  '<li>Search the pronunciations of words.</li>' +
  '<li>Switch between local voices of IELTS (British & Autralian) and TOEFL (American)</li>' +
  '<li>Changes size of text</li>' +
  '<li>Changes speed of speaking</li>' +
  '<li>Natural voice (*internet required)</li>' +
  '</ul>',
  localeTip: 'Cannot hear the difference pronunciations between US - GB - AU ? Please visit How-To page for more information.',
  db_location_default: 'default',
  LIMIT_DEFAULT: 30,
  LIMIT_MORE: 60,
  history_limit: 1000,
  default_speak_speed: 'Speed is ',
  app_rate_android: 'market://details?id=com.bibooki.mobile.dictionary.pronunciation.google',
  app_rate_ios: '1089137422',
  admobIds: {
    android: {
      banner: 'ca-app-pub-6148713949526588/8433942110',
      interstitial: 'ca-app-pub-6148713949526588/9910675311'
    },
    ios: {
      banner: 'ca-app-pub-6148713949526588/6817608113',
      interstitial: 'ca-app-pub-6148713949526588/2247807717'
    }
  },
  is_display_ads: true,
  google_analytics: 'UA-29094709-18',
  ttsAPI: '9d1a7a93c5844579a9240b1ec0f3e634',
  ttsUrlVoiceRSS: 'http://api.voicerss.org/?key=9d1a7a93c5844579a9240b1ec0f3e634',
  ttsUrlIBM: 'https://text-to-speech-demo.mybluemix.net/api/synthesize?download=true&accept=audio/ogg&',
  ttsVoiceUS: ['en-US_AllisonVoice', 'en-US_LisaVoice', 'en-US_MichaelVoice'],
  ttsVoiceGB: ['en-GB_KateVoice']
};
// https://www.ibm.com/watson/developercloud/doc/text-to-speech/http.html#voices

export const TABLE = {
  HISTORY: 'history',
  BOOKMARK: 'bookmark',
};

export const PLATFORM = {
  ios: 'ios',
  android: 'android'
};

export const COLORS = {
  white: {
    bg: 'rgb(255, 255, 255)',
    color: 'rgb(0, 0, 0)',
    highlight: 'rgba(255, 255, 0, 0.3)'
  },
  tan: {
    bg: 'rgb(249, 241, 228)',
    color: 'rgb(0, 0, 0)',
    highlight: 'rgba(255, 255, 255, 0.8)'
  },
  grey: {
    bg: '#555555',
    color: 'rgb(255, 255, 255)',
    highlight: 'rgba(142, 142, 142, 0.8)'
  },
  black: {
    bg: 'rgb(0, 0, 0)',
    color: 'rgb(255, 255, 255)',
    highlight: 'rgba(142, 142, 142, 0.8)'
  }
};

export const DICTS = {
  remote_deploy: {
    url_primary: 'http://public.bibooki.com/databases/dicts/pron_dicts.zip',
    url_minor: 'https://www.dropbox.com/s/btirfo9casce9h6/pron_dicts.zip?dl=1',
    file_name: 'pron_dicts.zip'
  },
  CMU_07b: {
    'code': 'CMU_07b',
    'dbFileName': 'cmudict_ipa.SQLite3',
    'count': 133854,
    'label': 'The Carnegie Mellon University Pronouncing Dictionary',
    'description': 'For North American English <b>(US only, TOEFL)</b> that contains 8,403 pronunciations of words, which modified and customized by LHViet88@gmail.com.'
  },
  CUV2: {
    'code': 'CUV2',
    'dbFileName': 'cuv2dict_ipa.SQLite3',
    'count': 70646,
    'label': 'A Computer-Usable Dictionary File Based On The Oxford Advanced Learner\'s Dictionary Of Current English',
    'description': 'For British English <b>(IELTS)</b> that contains over 7,701 pronunciations of words, which modified and customized by LHViet88@gmail.com.'
  }
};

export const LOCALES = {
  'GB': {
    'code': 'GB',
    'name': 'Great Britain',
    'short': '(IELTS - British)',
    'description': 'IELTS Pronunciation (British - United Kingdom)'
  },
  'US': {
    'code': 'US',
    'name': 'United States',
    'short': '(TOEFL - American)',
    'description': 'TOEFL Pronunciation (American - United States)'
  },
  'AU': {
    'code': 'AU',
    'name': 'Australia',
    'short': '(IELTS - Australian)',
    'description': 'Australian (pronunciation) voice'
  }
};

export const PRONUNCIATION = {
  'longest_char_number': 5,
  'consonants': {
    'b': {
      'word': '<span class=\'decor-underline\'>b</span>ig',
      'pron': '/bɪɡ/'
    },
    'd': {
      'word': '<span class=\'decor-underline\'>d</span>ig',
      'pron': '/dɪɡ/'
    },
    'dʒ': {
      'word': '<span class=\'decor-underline\'>j</span>et, <span class=\'decor-underline\'>j</span>u<span class=\'decor-underline\'>dg</span>e',
      'pron': '/dʒɛt/',
      'code': 'ipa'
    },
    'ð': {
      'word': '<span class=\'decor-underline\'>th</span>en',
      'pron': '/ðɛn/',
      'code': 'ipa'
    },
    'f': {
      'word': '<span class=\'decor-underline\'>f</span>ig',
      'pron': '/fɪɡ/'
    },
    'g': {
      'word': '<span class=\'decor-underline\'>g</span>et',
      'pron': '/ɡɛt/'
    },
    'h': {
      'word': '<span class=\'decor-underline\'>h</span>ow',
      'pron': '/haʊ/'
    },
    'j': {
      'word': '<span class=\'decor-underline\'>y</span>es, <span class=\'decor-underline\'>y</span>ou',
      'pron': '/jɛs/',
      'code': 'ipa'
    },
    'k': {
      'word': '<span class=\'decor-underline\'>k</span>it',
      'pron': '/kɪt/'
    },
    'l': {
      'word': '<span class=\'decor-underline\'>l</span>eg',
      'pron': '/lɛɡ/'
    },
    'm': {
      'word': '<span class=\'decor-underline\'>m</span>ain',
      'pron': '/meɪn/'
    },
    'n': {
      'word': '<span class=\'decor-underline\'>n</span>et',
      'pron': '/nɛt/'
    },
    'ŋ': {
      'word': 'thi<span class=\'decor-underline\'>ng</span>',
      'pron': '/θɪŋ/',
      'code': 'ipa'
    },
    'p': {
      'word': '<span class=\'decor-underline\'>p</span>it',
      'pron': '/pɪt/'
    },
    'r': {
      'word': '<span class=\'decor-underline\'>r</span>ain',
      'pron': '/reɪn/'
    },
    'ɹ': {
      'word': '<span class=\'decor-underline\'>r</span>un',
      'pron': '/r\'ʌn/'
    },
    's': {
      'word': '<span class=\'decor-underline\'>c</span>ity',
      'pron': '/s\'ɪti/',
      'code': 'ipa'
    },
    'ʃ': {
      'word': '<span class=\'decor-underline\'>sh</span>ip',
      'pron': '/ʃɪp/',
      'code': 'ipa'
    },
    't': {
      'word': '<span class=\'decor-underline\'>t</span>ame',
      'pron': '/teɪm/'
    },
    'tʃ': {
      'word': '<span class=\'decor-underline\'>ch</span>ip, <span class=\'decor-underline\'>ch</span>ur<span class=\'decor-underline\'>ch</span>',
      'pron': '/tʃɪp/',
      'code': 'ipa'
    },
    'θ': {
      'word': '<span class=\'decor-underline\'>th</span>in',
      'pron': '/θɪn/',
      'code': 'ipa'
    },
    'v': {
      'word': '<span class=\'decor-underline\'>v</span>et',
      'pron': '/vɛt/'
    },
    'w': {
      'word': '<span class=\'decor-underline\'>w</span>in',
      'pron': '/wɪn/'
    },
    'z': {
      'word': '<span class=\'decor-underline\'>z</span>ip',
      'pron': '/zɪp/'
    },
    'ʒ': {
      'word': 'vi<span class=\'decor-underline\'>s</span>ion',
      'pron': '/ˈvɪʒ(ə)n/',
      'code': 'ipa'
    },
    'x': {
      'locale': '(Scottish)',
      'word': 'lo<span class=\'decor-underline\'>ch</span>',
      'pron': '/lɒx/'
    },
    'ɬ': {
      'locale': '(Welsh)',
      'word': 'peni<span style=\'text-decoration: underline;\'>ll</span>ion',
      'pron': '/pɛˈnɪɬɪən/'
    }
  },
  'vowels': {
    'GB': {
      'iː': {
        'word': 'fl<span class=\'decor-underline\'>ee</span>ce',
        'code': 'ipa'
      },
      'i': {
        'word': 'happ<span class=\'decor-underline\'>y</span>'
      },
      'ɪ': {
        'word': 'k<span class=\'decor-underline\'>i</span>t',
        'code': 'ipa'
      },
      'ɛ': {
        'word': 'dr<span class=\'decor-underline\'>e</span>ss'
      },
      'a': {
        'word': 'c<span class=\'decor-underline\'>a</span>rry, tr<span class=\'decor-underline\'>a</span>p'
      },
      'ɑː': {
        'word': 'f<span class=\'decor-underline\'>a</span>ther',
        'code': 'ipa'
      },
      'ɒ': {
        'word': 'l<span class=\'decor-underline\'>o</span>t',
        'code': 'ipa'
      },
      'ɔː': {
        'word': 'h<span class=\'decor-underline\'>aw</span>k, f<span class=\'decor-underline\'>o</span>rce',
        'code': 'ipa'
      },
      'ʌ': {
        'word': 'c<span class=\'decor-underline\'>u</span>p',
        'code': 'ipa'
      },
      'ʊ': {
        'word': 'f<span class=\'decor-underline\'>oo</span>t',
        'code': 'ipa'
      },
      'uː': {
        'word': 'g<span class=\'decor-underline\'>oo</span>se',
        'code': 'ipa'
      },
      'ə': {
        'word': 'alph<span class=\'decor-underline\'>a</span>',
        'code': 'ipa'
      },
      'əː': {
        'word': 'n<span class=\'decor-underline\'>ur</span>se'
      },
      'ɜː': {
        'word': 'n<span class=\'decor-underline\'>ur</span>se',
        'code': 'ipa'
      },
      'ɪə': {
        'word': 'h<span class=\'decor-underline\'>ere</span>, n<span class=\'decor-underline\'>ea</span>r',
        'code': 'ipa'
      },
      'ɛː': {
        'word': 'squ<span class=\'decor-underline\'>are</span>'
      },
      'ʊə': {
        'word': 'c<span class=\'decor-underline\'>ure</span>',
        'code': 'ipa'
      },
      'jʊə': {
        'word': 'c<span class=\'decor-underline\'>ure</span>',
        'code': 'ipa'
      },
      'juː': {
        'word': 'c<span class=\'decor-underline\'>u</span>te',
        'code': 'ipa'
      },
      'eɪ': {
        'word': 'f<span class=\'decor-underline\'>a</span>ce',
        'code': 'ipa'
      },
      'ʌɪ': {
        'word': 'pr<span class=\'decor-underline\'>i</span>ce'
      },
      'aʊ': {
        'word': 'm<span class=\'decor-underline\'>ou</span>th',
        'code': 'ipa'
      },
      'əʊ': {
        'word': 'g<span class=\'decor-underline\'>oa</span>t',
        'code': 'ipa'
      },
      'ɔɪ': {
        'word': 'ch<span class=\'decor-underline\'>oi</span>ce',
        'code': 'ipa'
      },
      'ã': {
        'word': 'f<span class=\'decor-underline\'>in</span> de siècle'
      },
      'ɔ̃': {
        'word': 'b<span class=\'decor-underline\'>on</span> mot'
      }
    },
    'US': {
      'i': {
        'word': 'fl<span class=\'decor-underline\'>ee</span>ce, happ<span class=\'decor-underline\'>y</span>'
      },
      'ɪ': {
        'word': 'k<span class=\'decor-underline\'>i</span>t',
        'code': 'ipa'
      },
      'ɛ': {
        'word': 'dr<span class=\'decor-underline\'>e</span>ss, c<span class=\'decor-underline\'>a</span>rry'
      },
      'e': {
        'word': 'dr<span class=\'decor-underline\'>e</span>ss',
        'code': 'ipa'
      },
      'æ': {
        'word': 'tr<span class=\'decor-underline\'>a</span>p',
        'code': 'ipa'
      },
      'ɑː': {
        'word': 'f<span class=\'decor-underline\'>a</span>ther, l<span class=\'decor-underline\'>o</span>t',
        'code': 'ipa'
      },
      'ɔː': {
        'word': 'h<span class=\'decor-underline\'>aw</span>k',
        'code': 'ipa'
      },
      'ɑ': {
        'word': 'h<span class=\'decor-underline\'>aw</span>k'
      },
      'ə': {
        'word': 'c<span class=\'decor-underline\'>u</span>p, alph<span class=\'decor-underline\'>a</span>',
        'code': 'ipa'
      },
      'ʊ': {
        'word': 'f<span class=\'decor-underline\'>oo</span>t',
        'code': 'ipa'
      },
      'u': {
        'word': 'g<span class=\'decor-underline\'>oo</span>se'
      },
      'ɔr': {
        'word': 'f<span class=\'decor-underline\'>o</span>rce'
      },
      'ɔ': {
        'word': 'fireb<span class=\'decor-underline\'>a</span>ll'
      },
      'ər': {
        'word': 'n<span class=\'decor-underline\'>ur</span>se'
      },
      'ɜ': {
        'word': 'n<span class=\'decor-underline\'>ur</span>se'
      },
      'ɪ(ə)r': {
        'word': 'h<span class=\'decor-underline\'>ere</span>'
      },
      'ɛ(ə)r': {
        'word': 'squ<span class=\'decor-underline\'>are</span>'
      },
      'eə': {
        'word': 'squ<span class=\'decor-underline\'>are</span>',
        'code': 'ipa'
      },
      'ʊ(ə)r': {
        'word': 'c<span class=\'decor-underline\'>ure</span>'
      },
      'eɪ': {
        'word': 'f<span class=\'decor-underline\'>a</span>ce',
        'code': 'ipa'
      },
      'aɪ': {
        'word': 'pr<span class=\'decor-underline\'>i</span>ce',
        'code': 'ipa'
      },
      'aʊ': {
        'word': 'm<span class=\'decor-underline\'>ou</span>th',
        'code': 'ipa'
      },
      'oʊ': {
        'word': 'g<span class=\'decor-underline\'>oa</span>t'
      },
      'ɔɪ': {
        'word': 'ch<span class=\'decor-underline\'>oi</span>ce',
        'code': 'ipa'
      },
      'ɑ̃': {
        'word': 'f<span class=\'decor-underline\'>in</span> de siècle'
      },
      'ɔ̃': {
        'word': 'b<span class=\'decor-underline\'>on</span> mot'
      },
      'ɜr': {
        'word': 'abs<span class=\'decor-underline\'>ur</span>d',
        'pron': 'əbs\'ɜrd'
      },
      'ɝ': {
        'word': 'b<span class=\'decor-underline\'>ir</span>d',
        'pron': 'b\'ɜrdz'
      },
      'ɚ': {
        'word': 'cow<span class=\'decor-underline\'>ar</span>d',
        'pron': 'k\'aʊɜrd'
      },
      'ɛr': {
        'word': 'wh<span class=\'decor-underline\'>ere</span>',
        'pron': 'w\'er'
      },
      'ʊr': {
        'word': 'b<span class=\'decor-underline\'>ur</span>eau',
        'pron': 'bj\'ʊroʊ'
      },
      'ɑr': {
        'word': 'l<span class=\'decor-underline\'>ar</span>ge',
        'pron': 'l\'ɑrdʒ'
      },
      'ɪr': {
        'word': '<span class=\'decor-underline\'>ear</span>',
        'pron': '\'ir'
      },
      'aʊr': {
        'word': 'fl<span class=\'decor-underline\'>ower</span>',
        'pron': '\'fl\'aʊɜr'
      }
    }
  }
};

export const STORE = {
  version: 'version',
  locale: 'locale',
  locales: 'locales',
  isPopover: 'is_popup_show',
  isIPA: 'is_ipa_display',
  isRandom: 'is_word_random',
  fontsize: 'font_size',
  speed: 'speak_speed',
  color: 'color',
  isDual: 'is_dual_transcript'
};

export const DEFAULT = {
  locale: LOCALES.US.code,
  locales: [
    LOCALES.US.code,
    LOCALES.GB.code
  ],
  locales_full: [
    LOCALES.US.code,
    LOCALES.GB.code,
    LOCALES.AU.code
  ],
  isPopover: true,
  isIPA: true,
  isRandom: true,
  fontsize: 62.5,
  speed: 1,
  color: 'white',
  isDual: false
}

